package com.ticatag.background_location;

import android.location.Location;
import android.location.LocationManager;
import android.location.OnNmeaMessageListener;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.annotation.TargetApi;
import android.content.SharedPreferences;

import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationSettingsRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.EventChannel.EventSink;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.view.FlutterCallbackInformation;
import io.flutter.view.FlutterMain;
import io.flutter.view.FlutterNativeView;
import io.flutter.view.FlutterRunArguments;

/**
 * HeadlessPlugin
 */
public class HeadlessPlugin implements MethodCallHandler {
    private static final String METHOD_CHANNEL_NAME_BACKGROUND = "plugins.flutter.io/background_location_callback";
    private static final String TAG = "BackgroundLocation";

    final static String HANDLER_KEY = "background_location_handler";
    final static String CALLBACK_KEY = "background_location_callback";

    private static ArrayDeque<List<Object>> queue = new ArrayDeque<List<Object>>();

    private static LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private PluginRegistry.RequestPermissionsResultListener mPermissionsResultListener;

    private EventChannel backgroundChannel;

    @TargetApi(Build.VERSION_CODES.N)
    private OnNmeaMessageListener mMessageListener;

    private static Double mLastMslAltitude;

    // Parameters of the request
    private static long update_interval_in_milliseconds = 5000;
    private static long fastest_update_interval_in_milliseconds = update_interval_in_milliseconds / 2;
    private static Integer location_accuray = LocationRequest.PRIORITY_HIGH_ACCURACY;
    private static float distanceFilter = 0f;


    private static MethodChannel channel;
    private static MethodChannel mBackgroundChannel;
    private static PluginRegistry.PluginRegistrantCallback mPluginRegistrantCallback;

    private EventSink events;
    private Result result;

    private int locationPermissionState;

    private boolean waitingForPermission = false;
    private LocationManager locationManager;

    private Context mContext;

    private HashMap<Integer, Integer> mapFlutterAccuracy = new HashMap<>();

    private static FlutterNativeView mFlutterNativeView;

    private static final AtomicBoolean mSynchronizer = new AtomicBoolean(false);

    HeadlessPlugin(Context context) {
        this.mContext = context;
        if (mFlutterNativeView == null) {
            initFlutterNativeView();
        }
        synchronized(mSynchronizer) {
            if (!mSynchronizer.get()) {
                // Queue up events while background isolate is starting
                Log.d(TAG, "Waiting for Flutter Native View");
                return;
            }
        }
    }

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        HeadlessPlugin locationWithMethodChannel = new HeadlessPlugin(registrar.context());
    }

    @Override
    public void onMethodCall(MethodCall call, final Result result) {
        result.notImplemented();
    }

    // Called by Application#onCreate
    static void setPluginRegistrant(PluginRegistry.PluginRegistrantCallback callback) {
        Log.i(TAG, "PluginRegistrantCallback");
        mPluginRegistrantCallback = callback;
    }


    public static HashMap<String, Double> locationToHashMap(Location location) {
        HashMap<String, Double> loc = new HashMap<>();
        loc.put("latitude", location.getLatitude());
        loc.put("longitude", location.getLongitude());
        loc.put("accuracy", (double) location.getAccuracy());

        // Using NMEA Data to get MSL level altitude
        if (mLastMslAltitude == null || Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            loc.put("altitude", location.getAltitude());
        } else {
            loc.put("altitude", mLastMslAltitude);
        }

        loc.put("speed", (double) location.getSpeed());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            loc.put("speed_accuracy", (double) location.getSpeedAccuracyMetersPerSecond());
        }
        loc.put("heading", (double) location.getBearing());
        loc.put("time", (double) location.getTime());

        return loc;
    }

    public void handleNewBackgroundLocations(Context context, Location location) {

        HashMap<String, Double> locationMap = locationToHashMap(location);

        SharedPreferences prefs = context.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        Long mCallbackHandle = prefs.getLong(CALLBACK_KEY, -1);

        HashMap locationEventMap = new HashMap();
        locationEventMap.put("callbackHandle", mCallbackHandle);
        locationEventMap.put("location", locationMap);

        if (mBackgroundChannel != null) {
            mBackgroundChannel.invokeMethod("onLocationEvent", locationEventMap);
        } else {
            Log.e(TAG, "No channel :'('");
            this.initFlutterNativeView();
        }
    }

    private void initFlutterNativeView() {
        FlutterMain.ensureInitializationComplete(mContext, null);
        Log.i(TAG, "InitFlutterView");
        SharedPreferences prefs = mContext.getSharedPreferences(TAG, Context.MODE_PRIVATE);
        Long mCallbackHandle = prefs.getLong(HANDLER_KEY, -1);
        Log.i(TAG, "CallbackHandle: " + String.valueOf(mCallbackHandle));

        FlutterCallbackInformation callbackInfo = FlutterCallbackInformation
                .lookupCallbackInformation(mCallbackHandle);

        if (callbackInfo == null) {
            Log.e(TAG, "Fatal: failed to find callback");
            return;
        }

        mFlutterNativeView = new FlutterNativeView(mContext.getApplicationContext(), true);

        // Create the Transmitter channel
        mBackgroundChannel = new MethodChannel(mFlutterNativeView, METHOD_CHANNEL_NAME_BACKGROUND);
        mBackgroundChannel.setMethodCallHandler(this);

        if (mPluginRegistrantCallback == null) {
            return;
        }
        mPluginRegistrantCallback.registerWith(mFlutterNativeView.getPluginRegistry());

        // Dispatch back to client for initialization.
        FlutterRunArguments args = new FlutterRunArguments();
        args.bundlePath = FlutterMain.findAppBundlePath(mContext);
        args.entrypoint = callbackInfo.callbackName;
        args.libraryPath = callbackInfo.callbackLibraryPath;
        mFlutterNativeView.runFromBundle(args);
        mSynchronizer.set(true);
    }
}