package com.ticatag.background_location;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.List;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import static com.ticatag.background_location.HeadlessPlugin.CALLBACK_KEY;
import static com.ticatag.background_location.HeadlessPlugin.HANDLER_KEY;

/** BackgroundLocationPlugin */
public class BackgroundLocationPlugin implements MethodCallHandler, PluginRegistry.ActivityResultListener {
  private static final int REQUEST_FINE_LOCATION_PERMISSIONS = 1453;
  private static final String TAG = "BgLocationPlugin";

  private boolean waitingForPermission = false;
  static Registrar mRegistrar;
  private Context mContext;
  private final Activity activity;
  private PluginRegistry.RequestPermissionsResultListener mPermissionsResultListener;
  private int locationPermissionState;
  private Result result;

  private final FusedLocationProviderClient mFusedLocationClient;
  private final SettingsClient mSettingsClient;

  BackgroundLocationPlugin(Context context, Activity activity) {
    this.activity=activity;
    this.mContext=context;
    mSettingsClient = LocationServices.getSettingsClient(activity);
    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
    createPermissionsResultListener();
  }

  public static void setPluginRegistrant(PluginRegistry.PluginRegistrantCallback callback) {
    HeadlessPlugin.setPluginRegistrant(callback);
  }


  public static void registerWith(Registrar registrar) {

    if(registrar.activity() != null) {
      mRegistrar=registrar;
      final MethodChannel channel = new MethodChannel(registrar.messenger(), "plugins.flutter.io/background_location");

      final BackgroundLocationPlugin backgroundLocationPlugin = new BackgroundLocationPlugin(registrar.context(), registrar.activity());
      channel.setMethodCallHandler(backgroundLocationPlugin);

      registrar.addRequestPermissionsResultListener(backgroundLocationPlugin.getPermissionsResultListener());
    }

  }

  @Override
  public void onMethodCall(MethodCall call, Result result) {
    if (call.method.equals("getPlatformVersion")) {
      result.success("Android " + android.os.Build.VERSION.RELEASE);
    } else if (call.method.equals("startHeadlessService")) {
        Object arguments = call.arguments;
        Long arg = null;
        if (arguments instanceof  Integer) {
          arg = ((Integer)arguments).longValue();
        } else {
          arg = (Long)arguments;
        }
        startHeadlessService(arg);
        result.success(null);
    } else if (call.method.equals("singleLocation")) {
      Object arguments = call.arguments;
      Long arg = null;
      if (arguments instanceof  Integer) {
        arg = ((Integer)arguments).longValue();
      } else {
        arg = (Long)arguments;
      }
      singleLocation(arg);
      result.success(null);
    }else {
      result.notImplemented();
    }
  }

  void singleLocation(Long callbackRaw) {
    if (!checkPermissions()) {
      requestPermissions();
    }

    SharedPreferences prefs = mContext.getSharedPreferences("BackgroundLocation", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putLong(CALLBACK_KEY, callbackRaw);

    editor.apply();

    final LocationRequest locationRequest = LocationRequest.create();
    locationRequest.setNumUpdates(1);

    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
            .setAlwaysShow(true);


    Task<LocationSettingsResponse> task = mSettingsClient.checkLocationSettings(builder.build());

    task.addOnSuccessListener(activity, new OnSuccessListener<LocationSettingsResponse>() {
      @Override
      public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
        mFusedLocationClient.requestLocationUpdates(locationRequest, getBackgroundPendingIntent());
      }
    });

    task.addOnFailureListener(activity, new OnFailureListener() {
      @Override
      public void onFailure(Exception e) {
        Log.e(TAG, e.getMessage());
      }
    });


    //mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback,  Looper.getMainLooper());

  }

  private PendingIntent getBackgroundPendingIntent() {
    Intent intent = new Intent(mContext, com.ticatag.background_location.BackgroundLocationBroadcastReceiver.class);
    intent.setAction(com.ticatag.background_location.BackgroundLocationBroadcastReceiver.ACTION_PROCESS_UPDATES);
    return PendingIntent.getBroadcast(mContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
  }


  void startHeadlessService(Long handlerRaw) {
    SharedPreferences prefs = mContext.getSharedPreferences("BackgroundLocation", Context.MODE_PRIVATE);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putLong(HANDLER_KEY, handlerRaw);
    editor.apply();
  }


  public PluginRegistry.RequestPermissionsResultListener getPermissionsResultListener() {
    return mPermissionsResultListener;
  }

  private void createPermissionsResultListener() {
    mPermissionsResultListener = new PluginRegistry.RequestPermissionsResultListener() {
      @Override
      public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_FINE_LOCATION_PERMISSIONS && permissions.length == 1 && permissions[0].equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
          if (waitingForPermission) {
            waitingForPermission = false;
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
              result.success(1);
            } else {
              result.success(0);
            }
            result = null;
          }
          if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (result != null) {
             ;
            }
          } else {
            if (!shouldShowRequestPermissionRationale()) {
              if (result != null) {
                result.error("PERMISSION_DENIED_NEVER_ASK", "Location permission denied forever- please open app settings", null);
              }
            } else {
              if (result != null) {
                result.error("PERMISSION_DENIED", "Location permission denied", null);
              }
            }
          }
          return true;
        }
        return false;
      }
    };
  }

  private boolean checkPermissions() {
    this.locationPermissionState = ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION);
    return this.locationPermissionState == PackageManager.PERMISSION_GRANTED;
  }

  private void requestPermissions() {
    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
            REQUEST_FINE_LOCATION_PERMISSIONS);
  }

  private boolean shouldShowRequestPermissionRationale() {
    return ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.ACCESS_COARSE_LOCATION);
  }

  @Override
  public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
      case 1:
        if (resultCode == Activity.RESULT_OK) {
          this.result.success(1);
        } else {
          this.result.success(0);
        }
        break;
      default:
        return false;
    }
    return true;
  }


/*

  - (void)onLocationEvent:(CLLocation *)location {
  [_callbackChannel invokeMethod:@"onLocationEvent"
    arguments:@[
    @(_onLocationUpdateHandle), @(location.timestamp.timeIntervalSince1970),
    @(location.coordinate.latitude), @(location.coordinate.longitude),
    @(location.horizontalAccuracy), @(location.speed)
                       ]];
  }

*/
}
