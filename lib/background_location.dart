import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

enum LocationActivityType {
  other,
  automotiveNavigation,
  fitness,
  otherNavigation,
}

class Location {
  Location({double time, double latitude, double longitude, double altitude, double speed}) :
    this.time = DateTime.fromMillisecondsSinceEpoch(time.round(), isUtc: true),
    this.latitude=latitude,
    this.longitude=longitude,
    this.altitude=altitude,
    this.speed=speed {
    print("====>TIME: ${this.time.toIso8601String()}");
  }


  factory Location.fromJson(String jsonLocation) {
    final Map<String, dynamic> location = json.decode(jsonLocation);
    final double time = DateTime.parse(location['time']).toUtc().millisecondsSinceEpoch.toDouble();
    return Location(time: time, latitude: location['latitude'],
        longitude: location['longitude'], altitude: location['altitude'], speed: location['speed']);
  }

  factory Location.fromMap(Map<dynamic, dynamic> map) {
    print("===>MAP ${map} Lat: ${map['latitude']}");
    return Location(time: map['time'], latitude: map['latitude'],
        longitude: map['longitude'], altitude: map['altitude'], speed: map['speed']);
  }

  final DateTime time;
  final double latitude;
  final double longitude;
  final double altitude;
  final double speed;

  @override
  String toString() =>
      '[$time] ($latitude, $longitude) altitude: $altitude m/s: $speed';

  String toJson() {
    final Map<String, dynamic> location = <String, dynamic>{
      'time': time.toIso8601String(),
      'latitude': latitude,
      'longitude': longitude,
      'altitude': altitude,
      'speed': speed,
    };
    return json.encode(location);
  }
}

class BackgroundLocation {
  static const MethodChannel _channel =
      const MethodChannel('plugins.flutter.io/background_location');

  BackgroundLocation(
      {this.pauseLocationUpdatesAutomatically = false,
      this.showsBackgroundLocationIndicator = true,
      this.activityType = LocationActivityType.other}) {
    print("Starting LocationBackgroundPlugin service");
    final CallbackHandle handle =
        PluginUtilities.getCallbackHandle(_backgroundCallbackDispatcher);
    //_channel
      //  .invokeMethod(_kStartHeadlessService, <dynamic>[handle.toRawHandle()]);

    _channel.invokeMethod(_kStartHeadlessService, handle.toRawHandle());
  }

  static const String _kCancelLocationUpdates = 'cancelLocationUpdates';
  static const String _kMonitorLocationChanges = 'monitorLocationChanges';
  static const String _kSingleLocation = 'singleLocation';
  static const String _kStartHeadlessService = 'startHeadlessService';

  bool pauseLocationUpdatesAutomatically;
  bool showsBackgroundLocationIndicator;
  LocationActivityType activityType;

  Future<bool> getSingleLocation(void Function(Location location) callback) {
    if (callback == null) {
      throw ArgumentError.notNull('callback');
    }
    final CallbackHandle handle = PluginUtilities.getCallbackHandle(callback);

    return _channel.invokeMethod(_kSingleLocation, handle.toRawHandle()).then<bool>((dynamic result) => result);
  }

  Future<bool> monitorSignificantLocationChanges(
      void Function(Location location) callback) {
    if (callback == null) {
      throw ArgumentError.notNull('callback');
    }
    final CallbackHandle handle = PluginUtilities.getCallbackHandle(callback);
    return _channel.invokeMethod(_kMonitorLocationChanges, <dynamic>[
      handle.toRawHandle(),
      pauseLocationUpdatesAutomatically,
      showsBackgroundLocationIndicator,
      activityType.index
    ]).then<bool>((dynamic result) => result);
  }

  Future<void> cancelLocationUpdates() =>
      _channel.invokeMethod(_kCancelLocationUpdates);

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}

void _backgroundCallbackDispatcher() {
  const String kOnLocationEvent = 'onLocationEvent';
  const MethodChannel _channel =
      MethodChannel('plugins.flutter.io/background_location_callback');
  WidgetsFlutterBinding.ensureInitialized();
  Function onLocationEvent;


  _channel.setMethodCallHandler((MethodCall call) async {
    Function _performCallbackLookup() {
      final callbackHandle=call.arguments['callbackHandle'];
      final CallbackHandle handle =
          CallbackHandle.fromRawHandle(callbackHandle);
      final Function closure = PluginUtilities.getCallbackFromHandle(handle);

      if (closure == null) {
        print('Fatal Error: Callback lookup failed!');
        exit(-1);
      }
      return closure;
    }

    if (call.method == kOnLocationEvent) {
      onLocationEvent ??= _performCallbackLookup();
      final Location location = Location.fromMap(call.arguments['location']);
      onLocationEvent(location);
    } else {
      assert(false, "No handler defined for method type: '${call.method}'");
    }
  });
}
